require './board.rb'
require './piece.rb'
require 'debugger'

class Game
  attr_accessor :current_player
  attr_reader :board

  def initialize
    @board = Board.new
    @current_player = :red
  end

  def play
    loop do
      begin
        self.board.render

        user_input = get_user_input
        move_seq = convert_user_input(user_input)
        pos_to_move = move_seq.shift
        current_piece = self.board[pos_to_move]

        raise "No piece at position you want to move" unless current_piece
        raise "Not your piece!" unless current_piece.color == current_player

        self.board[pos_to_move].perform_moves(move_seq)
        break if over?
        @current_player = (self.current_player == :red) ? :black : :red

      rescue InvalidMoveError
        puts "Invalid move error!"
      rescue StandardError => e
        puts "Error: #{e.message}"
        puts
        retry
      end
    end
    puts "#{@current_player.to_s.upcase} WINS! End of game." #Change
  end

  private

  def over?
    @board.pieces.all? {|piece| piece.color == @current_player}
  end

  def convert_user_input(user_input)
    user_input.split(" ").map do |coordinates|
      coordinates.split(",")
    end.map do |coordinates|
      x, y = coordinates
      [Integer(x), Integer(y)]
    end
  end

  def get_user_input
    puts "#{self.current_player.to_s.upcase}, Please enter your move sequence, with the moving piece's start position first."
    puts
    puts "3,3 4,4 6,4 <- This jumps the 5,0 piece over 4,1 piece and then over the 2,2 piece."
    gets.chomp
  end
end

g = Game.new
g.play