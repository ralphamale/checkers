require 'colorize'

class Board
  def initialize(fill_grid = true)
    make_starting_grid(fill_grid)
  end

  def add_piece(piece)
    self[piece.pos] = piece
  end

  def render
    print "   "
    8.times { |i| print " #{i} " }
    puts

    bg_color = :light_black
    @rows.each_with_index do |row, i|
      print " #{i} "
      row.each_with_index do |piece, j|
        bg_color = switch_background(bg_color)
        print ((piece.nil?) ? "   " : piece.symbol.colorize(:color => piece.color)).colorize(:background => bg_color)
      end.join (" ")
      bg_color = switch_background(bg_color)
      puts
    end
  end

  def empty?(pos)
    self[pos].nil?
  end

  def pieces
    @rows.flatten.compact
  end

  def dup
    duped_board = Board.new(false)

    pieces.each do |piece|
      Piece.new(piece.pos.dup, duped_board, piece.color, piece.king)
    end

    duped_board
  end

  def []=(pos,piece)
    x, y = pos
    @rows[x][y] = piece
  end

  def [](pos)
    x, y = pos
    @rows[x][y]
  end

  private

  def make_starting_grid(fill_grid)
    @rows = Array.new(8) { Array.new(8) }

    if fill_grid
      row_start_toggle = false

      8.times do |i|
        next if [3,4].include?(i)

        current_toggle = row_start_toggle
        8.times do |j|
          Piece.new([i,j], self, (i > 4) ? :black : :red) if current_toggle
          current_toggle = !current_toggle
        end
        row_start_toggle = !row_start_toggle
      end
    end
  end

  def switch_background(bg_color)
    (bg_color == :light_black) ? :white : :light_black
  end

end