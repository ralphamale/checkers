class Piece
  attr_accessor :pos, :king, :board, :color

  def initialize(pos, board, color, king = false)
    @pos = pos
    @board = board
    @color = color
    @king = king
    board.add_piece(self)
  end

  def symbol
    (self.king?) ? " \u265a " : " \u2022 "
  end

  def perform_moves(move_seq)
    if valid_move_seq?(move_seq)
      perform_moves!(move_seq)
    else
      raise InvalidMoveError
    end
  end

  def king?
    !!@king
  end

  protected

  def perform_moves!(move_seq)
    if move_seq.count == 1
      return if perform_slide(move_seq.first)
    end

    until move_seq.empty?
      current_pos = move_seq.shift
      raise InvalidMoveError if perform_jump(current_pos) == false
    end
  end

  def valid_move_seq?(move_seq)
    begin
      duped_board = board.dup
      duped_board[self.pos].perform_moves!(move_seq.dup)
    rescue
      return false
    end

    return true
  end

  def perform_slide(to_pos)
    if legal_move?(to_pos) && board.empty?(to_pos)

      board[to_pos] = self
      board[self.pos] = nil
      self.pos = to_pos
      maybe_promote

      return true
    end

    return false
  end

  def perform_jump(target_pos)
    if legal_move?(target_pos) && enemy_in_pos?(target_pos)
      board[target_pos] = nil #jumped piece to be removed
      board[self.pos] = nil

      new_pos = pos_after_jump(target_pos)
      board[new_pos] = self
      self.pos = new_pos

      maybe_promote
      return true
    end

    return false
  end

  def enemy_in_pos?(pos)
    !board.empty?(pos) && board[pos].color != self.color
  end

  def pos_after_jump(pos) # pos = piece to jump over
    x, y = self.pos
    dx, dy = [pos[0] - x, pos[1] - y]

    return [x + (2 * dx), y + (2 * dy)]
  end

  def move_dirs
    up_dir = [[-1, -1], [-1, 1]]
    down_dir = [[1, -1], [1, 1]]

    return (up_dir + down_dir) if self.king?

    (self.color == :red) ? down_dir : up_dir
  end

  def maybe_promote
    promo_row = (self.color == :black) ? 0 : 7
    self.king = true if self.pos[0] == promo_row
  end

  def legal_diagonal?(pos)
    move_dirs.any? do |(dx, dy)|
     [self.pos[0] + dx, self.pos[1] + dy] == pos
    end
  end

  private

  def in_bounds?(pos)
    pos.all? { |coord| coord.between?(0,7)}
  end

  def legal_move?(to_pos) #in coordinates
    in_bounds?(to_pos) && legal_diagonal?(to_pos)
  end

end

class InvalidMoveError < StandardError
end